require 'rubygems'
require 'net/ldap'
require 'cassandra-cql'
require 'logger'
require 'pp'

DATABASE_HOST = '127.0.0.1:9160'
DATABASE_NAME = 'otp_handler'

module LDAPServersCheck
  extend self

  def connect_to_database
    begin
      @database = CassandraCQL::Database::new(DATABASE_HOST)
      @database.execute("USE #{DATABASE_NAME}")
      @log.debug "Успешно подключился к базе данных"
      return true
    rescue => error
      @log.error "Ошибка при инициализации базы данных, Ошибка: #{error.message}"
      return false
    end
  end

  def update_ldap_servers_for(domain)
    begin
      @database.execute("UPDATE Domains SET ldap_servers = ?, disabled_ldap_servers = ? WHERE domain = ? ",
        domain[:ldap_servers],
        domain[:disabled_ldap_servers],
        domain[:domain_name]
      )
      return true
    rescue => error
      @log.error "Ошибка при обновлении домена: #{domain[:domain_name]}, Ошибка: #{error.message}"
      return false
    end
  end

  def initialize
    @log = Logger.new('ldap_check_log.txt')

    return unless connect_to_database

    @domains = []
    begin
      @database.execute('SELECT domain, ldap_base, admin_username, admin_password, ldap_servers, disabled_ldap_servers FROM DOMAINS').fetch do |row|
        all_ldap_servers = (row['ldap_servers'] || {}).merge (row['disabled_ldap_servers'] || {})
        @domains  << {
          domain_name:           row['domain'],
          ldap_base:             row['ldap_base'],
          admin_username:        row['admin_username'],
          admin_password:        row['admin_password'],
          ldap_servers:          row['ldap_servers'] || {},
          disabled_ldap_servers: row['disabled_ldap_servers'] || {},
          all_ldap_servers:      all_ldap_servers
        }
      end
    rescue => error
      @log.error "Ошибка при получении доменов из базы данных! Домен #{domain[:domain_name]}, Ошибка: #{error.message}"
      return false
    end
    return true
  end

  def create_ldap_connection(ldap_host, use_ssl, credentials)
    Net::LDAP.new host: ldap_host.split(':')[0],
                  port: ldap_host.split(':')[1],
                  encryption: use_ssl ? :simple_tls : nil,
                  auth: {
                    method: :simple,
                    username: credentials[:admin_username],
                    password: credentials[:admin_password]
                  }
  end


  def check_server(ldap_host, use_ssl, credentials)
    begin
      return :live if create_ldap_connection(ldap_host, use_ssl, credentials).bind
      return :dead
    rescue => error
      @log.error "Ошибка при подключении к LDAP серверу"
      @log.error error.message
      return :dead
    end
    :dead
  end

  # Проверяем ldap сервера для домена
  def inspect_ldap_servers_for(domain)
    domain[:all_ldap_servers].each do |ldap_host_and_port, ldap_ssl|
      server_status = check_server(ldap_host_and_port, ldap_ssl, domain)
      if server_status == :dead
        domain[:ldap_servers].delete(ldap_host_and_port)
        domain[:disabled_ldap_servers].merge!( { ldap_host_and_port => ldap_ssl })
      elsif server_status == :live
        domain[:disabled_ldap_servers].delete(ldap_host_and_port)
        domain[:ldap_servers].merge!( { ldap_host_and_port => ldap_ssl }) unless domain[:ldap_servers].has_key?(ldap_host_and_port)
      end
    end
    update_ldap_servers_for domain
  end

  def start_checking
    return false unless initialize

    @domains.each do |domain|
      thread = Thread.new do
        inspect_ldap_servers_for domain
      end
      thread.join
    end

  end

end

LDAPServersCheck.start_checking
